require 'try_robot/commands_builder'

describe TryRobot::CommandsBuilder do

  it 'builds one commands for each instruction' do
    builder = TryRobot::CommandsBuilder.new([
      'PLACE 3,2,NORTH', 'MOVE', 'RIGHT', 'LEFT', 'REPORT'
    ])

    commands = builder.commands
    expect(commands.size).to eq(5)
  end

  it 'ignores invalid instruction' do
    builder = TryRobot::CommandsBuilder.new([
      ' PLACE 3,2 ', ' MOOOVE ', 'RIGHT2', 'LEFTa', 'REPORT 2'
    ])

    commands = builder.commands
    expect(commands).to be_empty
  end

  context 'matching instructions' do
    it 'recognizes place instruction' do
      builder = TryRobot::CommandsBuilder.new([
        'PLACE 3,2,NORTH'
      ])

      commands = builder.commands
      expect(commands.size).to eq(1)
      expect(commands.first.class).to eq(TryRobot::Commands::Starter)
    end

    it 'recognizes move instruction' do
      builder = TryRobot::CommandsBuilder.new([
        'MOVE'
      ])

      commands = builder.commands
      expect(commands.size).to eq(1)
      expect(commands.first.class).to eq(TryRobot::Commands::Mover)
    end

    it 'recognizes rotate instruction' do
      builder = TryRobot::CommandsBuilder.new([
        'RIGHT', 'LEFT'
      ])

      commands = builder.commands
      expect(commands.size).to eq(2)
      expect(commands.first.class).to eq(TryRobot::Commands::Rotator)
      expect(commands.last.class).to eq(TryRobot::Commands::Rotator)
    end

    it 'recognizes report instruction' do
      builder = TryRobot::CommandsBuilder.new([
        'REPORT'
      ])

      commands = builder.commands
      expect(commands.size).to eq(1)
      expect(commands.first.class).to eq(TryRobot::Commands::Reporter)
    end
  end
end
