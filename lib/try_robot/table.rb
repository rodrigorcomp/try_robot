module TryRobot
  class Table
    def initialize(side_length: 5)
      @length = side_length
      @width = side_length
    end

    def valid?(position)
      position.first_quadrant? && position.x < @length && position.y < @length
    end
  end
end
