require 'try_robot/table'

describe TryRobot::Table do
  context 'validating positions' do

    it 'checks for valid positions' do
      table = TryRobot::Table.new(side_length: 2)

      expect(table.valid?(TryRobot::Position.new(0,0))).to be(true)
      expect(table.valid?(TryRobot::Position.new(0,1))).to be(true)
      expect(table.valid?(TryRobot::Position.new(1,0))).to be(true)
      expect(table.valid?(TryRobot::Position.new(1,1))).to be(true)

      expect(table.valid?(TryRobot::Position.new(2,0))).to be(false)
      expect(table.valid?(TryRobot::Position.new(0,2))).to be(false)
    end

    it 'doesnt accept negative positions' do
      table = TryRobot::Table.new(side_length: 2)

      expect(table.valid?(TryRobot::Position.new(0, -1))).to be(false)
      expect(table.valid?(TryRobot::Position.new(-1, 0))).to be(false)
    end
  end
end
