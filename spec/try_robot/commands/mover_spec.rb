require 'try_robot/robot'
require 'try_robot/position'
require 'try_robot/commands/mover'

describe TryRobot::Commands::Mover do

  before do
    @robot = TryRobot::Robot.new
    @position = TryRobot::Position.new(1,1)
    @mover = TryRobot::Commands::Mover.new()
  end

  it 'moves towards to NORTH' do
    @robot.go_to @position, TryRobot::Direction.new(:NORTH)

    @mover.execute(@robot)
    expect(@robot.position).to eq(TryRobot::Position.new(1,2))
  end

  it 'moves towards to SOUTH' do
    @robot.go_to @position, TryRobot::Direction.new(:SOUTH)

    @mover.execute(@robot)
    expect(@robot.position).to eq(TryRobot::Position.new(1,0))
  end

  it 'moves towards to EAST' do
    @robot.go_to @position, TryRobot::Direction.new(:EAST)

    @mover.execute(@robot)
    expect(@robot.position).to eq(TryRobot::Position.new(2,1))
  end

  it 'moves towards to WEST' do
    @robot.go_to @position, TryRobot::Direction.new(:WEST)

    @mover.execute(@robot)
    expect(@robot.position).to eq(TryRobot::Position.new(0,1))
  end

end
