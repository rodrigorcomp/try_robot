require 'try_robot/robot'
require 'try_robot/position'
require 'try_robot/commands/rotator'

describe TryRobot::Commands::Rotator do

  before do
    @robot = TryRobot::Robot.new
    @robot.go_to TryRobot::Position.new(1,1), TryRobot::Direction.new(:NORTH)
  end

  context 'turning to left' do
    before do
      @rotator = TryRobot::Commands::Rotator.new('LEFT')
    end

    it 'rotates to left' do
      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:WEST)

      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:SOUTH)

      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:EAST)

      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:NORTH)
    end
  end

  context 'turning to right' do
    before do
      @rotator = TryRobot::Commands::Rotator.new('RIGHT')
    end

    it 'rotates to right' do
      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:EAST)

      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:SOUTH)

      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:WEST)

      @rotator.execute(@robot)
      expect(@robot.facing).to eq(:NORTH)
    end
  end
end
