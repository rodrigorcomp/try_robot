module TryRobot
  class Position
    attr_reader :x, :y

    def initialize(x, y)
      @x, @y = x.to_i, y.to_i
    end

    def first_quadrant?
      x >= 0 && y >= 0
    end

    def to_s
      [@x, @y].join ','
    end

    def ==(o)
      o.class == self.class && o.x == @x && o.y == @y
    end

    def next_towards(direction)
      case direction.facing
      when :NORTH
        Position.new(x, y+1)
      when :EAST
        Position.new(x+1, y)
      when :SOUTH
        Position.new(x, y-1)
      when :WEST
        Position.new(x-1, y)
      end
    end
  end
end
