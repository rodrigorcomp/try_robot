require 'try_robot/game'

describe TryRobot::Game do

  before do
    @game = TryRobot::Game.new
  end
  it 'runs valid instructions' do
    @game.run(
      [
        'PLACE 0,0,NORTH',
        'MOVE',
        'REPORT'
      ]
    )

    expect(@game.robot.reports).to eq(['0,1,NORTH'])
  end

  it 'ignores invalid instructions' do
    @game.run(
      [
        'PLACE 0,0,NORTH',
        '12MOVE',
        'MOVE',
        '',
        'REPORT'
      ]
    )

    expect(@game.robot.reports).to eq(['0,1,NORTH'])
  end

  it 'ignores all instructions before the first PLACE' do
    @game.run(
      [
        'MOVE',
        'REPORT',
        'LEFT',
        'RIGHT',
        'PLACE 0,0,NORTH',
        'REPORT'
      ]
    )

    expect(@game.robot.reports).to eq(['0,0,NORTH'])
  end

  it 'ignores moves outside the table' do
    @game.run(
      [
        'PLACE 4,4,NORTH',
        'MOVE',
        'MOVE',
        'REPORT'
      ]
    )

    expect(@game.robot.reports).to eq(['4,4,NORTH'])
  end

  it 'ignores when placed outside the table' do
    @game.run(
      [
        'PLACE 4,4,SOUTH',
        'MOVE',
        'RIGHT',
        'REPORT',
        'PLACE 9,9,NORTH',
        'MOVE',
        'REPORT'
      ]
    )

    expect(@game.robot.reports).to eq(['4,3,WEST', '3,3,WEST'])
  end
end
