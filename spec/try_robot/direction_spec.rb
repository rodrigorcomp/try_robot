require 'try_robot/direction'

describe TryRobot::Direction do
  context 'rotating' do
    it 'rotates to left' do
      expect(TryRobot::Direction::NORTH.turn_left).to eq(TryRobot::Direction::WEST)
      expect(TryRobot::Direction::WEST.turn_left).to eq(TryRobot::Direction::SOUTH)
      expect(TryRobot::Direction::SOUTH.turn_left).to eq(TryRobot::Direction::EAST)
      expect(TryRobot::Direction::EAST.turn_left).to eq(TryRobot::Direction::NORTH)
    end

    it 'rotates to right' do
      expect(TryRobot::Direction::NORTH.turn_right).to eq(TryRobot::Direction::EAST)
      expect(TryRobot::Direction::WEST.turn_right).to eq(TryRobot::Direction::NORTH)
      expect(TryRobot::Direction::SOUTH.turn_right).to eq(TryRobot::Direction::WEST)
      expect(TryRobot::Direction::EAST.turn_right).to eq(TryRobot::Direction::SOUTH)
    end
  end
end
