require 'try_robot/commands/base'

module TryRobot
  module Commands
    class Reporter < Base
      protected

      def _execute(robot)
        robot.report
      end
    end
  end
end
