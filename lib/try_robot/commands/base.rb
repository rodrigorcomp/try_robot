

module TryRobot
  module Commands
    class Base
      def initialize(*_args)
      end

      def execute(robot)
        _execute(robot)
      end

      protected

      def _execute(robot)
        raise 'Not implemented'
      end
    end
  end
end
