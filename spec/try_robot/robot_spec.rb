require 'try_robot/robot'

describe TryRobot::Robot do

  before do
    @robot = TryRobot::Robot.new
  end

  it 'doenst start before having a position' do
    expect(@robot).to_not be_started
  end

  it 'returns its facing direction' do
    expect(@robot.facing).to be_nil
    @robot.go_to(TryRobot::Position.new(2,3), TryRobot::Direction::NORTH)

    expect(@robot.facing).to eq :NORTH
  end

  it 'prints its currenct state' do
    expect(@robot.to_s).to eq ''

    @robot.go_to(TryRobot::Position.new(2,3), TryRobot::Direction::NORTH)

    expect(@robot.to_s).to eq '2,3,NORTH'
  end

  context 'reporting' do
    it 'stores its past positions' do
      expect(@robot.reports).to eq []
    end

    it 'doenst store when in an invalid position' do
      @robot.report
      expect(@robot.reports).to eq []
    end

    it 'stores all valid positions' do
      @robot.go_to(TryRobot::Position.new(2,3), TryRobot::Direction::NORTH)
      @robot.report
      @robot.move
      @robot.turn_right
      @robot.report

      expect(@robot.reports).to eq ['2,3,NORTH', '2,4,EAST']
    end
  end

  context 'rotating' do
    it 'do nothing before start' do
      expect(@robot.direction).to be_nil
      @robot.turn_right
      expect(@robot.direction).to be_nil
      @robot.turn_left
      expect(@robot.direction).to be_nil
    end

    it 'rotates to right' do
      @robot.go_to(TryRobot::Position.new(1,1), TryRobot::Direction::NORTH)
      @robot.turn_right
      expect(@robot.direction).to eq TryRobot::Direction::EAST
    end

    it 'rotates to left' do
      @robot.go_to(TryRobot::Position.new(1,1), TryRobot::Direction::NORTH)
      @robot.turn_left
      expect(@robot.direction).to eq TryRobot::Direction::WEST
    end
  end

  context 'moving' do
    it 'do nothing before start' do
      expect(@robot.position).to be_nil
      @robot.move
      expect(@robot.position).to be_nil
    end

    it 'moves to the facing direction' do
      @robot.go_to(TryRobot::Position.new(1,1), TryRobot::Direction::NORTH)
      @robot.move
      expect(@robot.to_s).to eq '1,2,NORTH'
    end
  end
end
