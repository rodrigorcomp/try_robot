require 'try_robot/position'
require 'try_robot/direction'

module TryRobot
  class Robot

    attr_reader :position, :direction, :reports

    def initialize()
      @reports = []
    end

    def started?
      !@position.nil? && !@direction.nil?
    end

    def facing
      @direction.facing if @direction
    end

    def move
      return unless started?
      @position = @position.next_towards(@direction)
    end

    def go_to(position, direction)
      @position = position
      @direction = direction
    end

    def turn_right
      return unless started?
      @direction = @direction.turn_right
    end

    def turn_left
      return unless started?
      @direction = @direction.turn_left
    end

    def report
      return unless started?
      @reports << to_s
    end

    def to_s
      [@position, @direction].compact.join(',')
    end
  end
end
