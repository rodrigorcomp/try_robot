require 'try_robot/commands/base'

module TryRobot
  module Commands
    class Starter < Base
      def initialize(x, y, direction)
        @position = Position.new(x, y)
        @direction = Direction.new(direction.to_sym)
      end

      protected

      def _execute(robot)
        robot.go_to(@position, @direction)
      end
    end
  end
end
