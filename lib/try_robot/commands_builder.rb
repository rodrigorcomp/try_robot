require 'try_robot/commands/starter'
require 'try_robot/commands/mover'
require 'try_robot/commands/rotator'
require 'try_robot/commands/reporter'

module TryRobot
  class CommandsBuilder

    COMMANDS = {
      /^PLACE\s(\d),(\d),(NORTH|SOUTH|WEST|EAST)$/ => Commands::Starter,
      /^MOVE$/ =>                                     Commands::Mover,
      /^(RIGHT|LEFT)$/ =>                             Commands::Rotator,
      /^REPORT$/ =>                                   Commands::Reporter,
    }.freeze

    def initialize(instructions)
      @instructions = instructions
    end

    def commands
      @instructions.map { |instruction| command_for(instruction) }.compact
    end

    private
    def command_for(instruction)
      command = COMMANDS.find { |regex, _| regex =~ instruction }

      if command
        regex, command_class = command

        match_data = regex.match(instruction)[1..-1]

        command_class.new(*match_data)
      end
    end
  end
end
