require 'try_robot/table'
require 'try_robot/robot'
require 'try_robot/commands_builder'

module TryRobot
  class Game
    attr_reader :robot

    def initialize(table: Table.new(side_length: 5), robot: Robot.new)
      @table = table
      @robot = robot
    end

    def run(instructions)
      commands = CommandsBuilder.new(instructions).commands
      run_commands(commands)
    end

    private

    def run_commands(commands)
      commands.each do |command|
        former_position  = @robot.position
        former_direction  = @robot.direction
        command.execute(@robot)

        unless robot_on_valid_position?
          # undo last move
          @robot.go_to(former_position, former_direction)
        end
      end
    end

    def robot_on_valid_position?
      @robot.started? && @table.valid?(@robot.position)
    end
  end
end
