# Try robot

TryRobot is a simulator of a toy robot moving on a square tabletop.


## Install

- Add it to your gemfile:

```
gem 'try_robot', git: 'git@gitlab.com:rodrigorcomp/try_robot.git'
```

## Usage

- Run it with raw input and printing the result to the standard output:

```
require 'try_robot'

input = "PLACE 2,2NORTH\nMOVE\nREPORT"

TryRobot.run(input) #=> prints: 2,3,NORTH
```

- Run the simulator by yourself:

```
require 'try_robot'

input = "PLACE 2,2NORTH\nMOVE\nREPORT"

game = TryRobot.Game.new
game.run(input.split("\n"))

game.robot # => returns the robot with its last position and reports
game.robot.position #=> 2,3
game.robot.facing # => :NORTH
game.robot.reports # => ['2,3,NORTH']
```

- Run it as a Rake task with a file as input:

```
rake try_robot['example/case3.txt']
```

## Examples

We've 3 input examples under `examples` folder:

- example/case1.txt
- example/case2.txt
- example/case3.txt

