require 'try_robot/game'

module TryRobot

  class Printer
    def self.announce(reports)
      puts reports
    end
  end

  def self.run(announcer: Printer, input:)
    instructions = input.split("\n")

    game = Game.new
    game.run(instructions)
    announcer.announce(game.robot.reports)
  end
end
