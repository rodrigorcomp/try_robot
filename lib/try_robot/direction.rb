module TryRobot
  class Direction
    attr_reader :facing

    VALID_DIRECTIONS = [:NORTH, :EAST, :SOUTH, :WEST].freeze

    def initialize(facing)
      raise "invalid direction #{facing}" unless VALID_DIRECTIONS.include?(facing)
      @facing = facing
    end

    def turn_right
      turn(1)
    end

    def turn_left
      turn(-1)
    end

    def to_s
      @facing.to_s
    end

    def ==(o)
      o.class == self.class && o.facing == @facing
    end

    VALID_DIRECTIONS.each do |direction|
      const_set(direction, new(direction))
    end

    private

    def turn(rotation_count)
      index = VALID_DIRECTIONS.index(@facing)

      new_facing = VALID_DIRECTIONS.rotate(rotation_count)[index]

      Direction.new(new_facing)
    end

  end
end
