require 'try_robot/commands/base'

module TryRobot
  module Commands
    class Mover < Base
      protected

      def _execute(robot)
        robot.move
      end
    end
  end
end
