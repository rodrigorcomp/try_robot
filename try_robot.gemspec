Gem::Specification.new do |s|
  s.name        = 'try_robot'
  s.version     = '0.0.1'
  s.date        = '2016-11-08'
  s.summary     = "TryRobot is a simulator of a toy robot moving on a square tabletop."
  s.description = "A simple hello world gem"
  s.authors     = ["Rodrigo Ra"]
  s.email       = 'rodrigo@email.com'
  s.files       = `git ls-files`.split($/)
  s.homepage    = 'http://rubygems.org/gems/try_robot'
  s.license     = 'MIT'
end
