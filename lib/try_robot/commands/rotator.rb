require 'try_robot/commands/base'

module TryRobot
  module Commands
    class Rotator < Base
      def initialize(direction)
        @direction = direction.upcase.to_sym
      end

      protected

      def _execute(robot)
        case @direction
        when :RIGHT
          robot.turn_right
        when :LEFT
          robot.turn_left
        end
      end
    end
  end
end
